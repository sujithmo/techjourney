package com.strings;
/*
 * Author: Sujith Mohan Velliyattikuzhi
 * 
 * Java program to count the number of particular character in a String
 */
public class CountCharactersInString {

	public static void main(String args[])
	{
		String str = "Hello How are you? Believe you are doing good.";
		
		System.out.println("Count of 'e' in string = "+returnCountOfCharacter('e', str));
		System.out.println("Count of 'h' in string = "+returnCountOfCharacter('h', str));
		System.out.println("Count of 'o' in string = "+returnCountOfCharacter('o', str));
	}
	
	public static int returnCountOfCharacter(char ch, String str)
	{
		if(str.isEmpty())
		{
			return 0;
		}
		
		else
		{
			int count = 0;
			str = str.toLowerCase();
			for(int i=0;i<str.length();i++)
			{
				if(ch==str.charAt(i))
				{
					count++;
				}
			}
			return count;
		}
	}
}
