package com.unitTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.programs.secondLargest;

public class testForSecondLargest {

	@Test
	public void testCases() {
		int arr[] = { 100, 6, 10, 7, 11, 12, 3, 2, 100, 17, 8, 9, 100, 91, 981,
				5 };
		assertEquals(100, secondLargest.findSecondLargest(arr));

		int arr2[] = { -40, -30, -17, -15, -23, 0, -31 };
		assertEquals(-15, secondLargest.findSecondLargest(arr2));

	}

}
