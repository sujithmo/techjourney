package com.arrays;

import java.util.HashSet;
import java.util.Set;

/*
 * Java program to return the first repeating value in an array
 */
public class firstRepeatingElement {

	public static void main(String[] args) {
		int arr[] = new int[]{1,4,3,7,6,4,8,1,3,7};
		
		if((firstRepeatingValue(arr))!=null)
		{
			System.out.println(firstRepeatingValue(arr));
		}
		else{
			System.out.println("No repeating values");
		}
	}
	
	private static Integer firstRepeatingValue(int arr[])
	{
		Set<Integer> set = new HashSet<Integer>();
		for(int i=0;i<arr.length;i++)
		{
			if(set.contains(arr[i]))
			{
				return arr[i];
			}
			
			else
			{
				set.add(arr[i]);
			}
		}
		return null;
		
	}
}
