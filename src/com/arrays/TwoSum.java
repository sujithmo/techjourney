package com.arrays;
import java.util.HashSet;
import java.util.Set;

/*
 * Java program to check whether given sum exists in array
 */

public class TwoSum {

	public static boolean twoSum(int[] array, int sum)
	{
		Set<Integer> set = new HashSet<Integer>();
		
		for(int value: array)
		{
			if(set.contains(sum-value))
			{
				return true;
			}
			set.add(value);
		}
		
		return false;
		
	}
	
	public static void main(String[] args) {
		
		int arr1[] = {2,5,4,7,8,3,6,10,4};
		System.out.println(twoSum(arr1,11));
	}
}
