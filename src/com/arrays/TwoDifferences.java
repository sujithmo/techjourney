package com.arrays;
import java.util.HashSet;
import java.util.Set;


/*
 * Java program to check whether given difference is found in an array.
 */
public class TwoDifferences {

	public static void main(String[] args) {
		
		int arr[] = {3,5,7,8,4,12,6,8};
		
		System.out.println(checkDifference(arr, 9)); //true
		System.out.println(checkDifference(arr,10)); //false
		
	}
	
	public static boolean checkDifference(int arr[], int diff)
	{
		Set<Integer> set = new HashSet<Integer>();
		
		for(int value: arr)
		{
			if(set.contains(value+diff)||(set.contains(value-diff)))
			{
				return true;
			}
			set.add(value);
		}
		return false;
	}
}
