package com.arrays;
/*
 * Java program to remove duplicate elements from sorted array
 */

public class RemoveDuplicatesFromSortedArrays {

	public static void main(String[] args) {
		int arr[] = {1,1,2,2,3,3,3,3,4,4,5,5,5,6,7,7};
		RemoveDups(arr);
	}
	public static void RemoveDups(int arr[])
	{
		int length = arr.length;
		int count=0;
		for(int i=1; i<length;i++)
		{
			if((arr[i]-arr[i-1])==0)
			{
				count++;
			}
		}
		
		int output[] = new int[length-count];
		count =0;
		int jcount = length-count;
		for(int i=1;i<length;i++)
		{
			if(arr[i]!=arr[i-1])
			{
				output[count++] = arr[i-1];
				jcount--;
			}
		}
		if(jcount!=0)
		{
			output[count]=arr[length-1];
		}
		for(int i:output)
		{
			System.out.print(i);
		}
	}
}
