package com.arrays;

public class FindSecondSmallestNumberInArray {

	public static void main(String[] args) {

		int arr[] = { -10, 5, 7, 9, -4, 0, -10, 11, -8, -10, 12, 6 };

		int num = returnSecondSmallest(arr);
		System.out.println(" Second smallest number in array = " + num);

	}

	public static int returnSecondSmallest(int ab[]) {
		int smallest = ab[0];
		int sum = 0;
		for (int i = 1; i < ab.length; i++) {
			if (ab[i] < smallest) {
				smallest = ab[i];
			}

		}

		int secondSmallest = Integer.MAX_VALUE;
		for (int i = 0; i < ab.length; i++) {
			if ((ab[i] < secondSmallest) && (ab[i] != smallest)) {
				secondSmallest = ab[i];
			}
		}

		return secondSmallest;
	}

}
