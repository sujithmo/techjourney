package com.programs;

/*
 * Java Program to check if the string contains duplicate characters or not in an efficient way.
 */

public class strdups {

	public static void main(String args[])

	{

		String textWithDuplicateCharacters = "aabbcdefghij";
		String textWithUniqueCharacters = "abcdefghijklmnopqrstuvwxyz";

		printOutput(isUnique(textWithDuplicateCharacters));
		printOutput(isUniqueEfficient(textWithDuplicateCharacters));

		System.out.println();

		printOutput(isUnique(textWithUniqueCharacters));
		printOutput(isUniqueEfficient(textWithUniqueCharacters));

	}

	public static boolean isUnique(String str)

	{

		boolean[] char_set = new boolean[256];

		for (int i = 0; i < str.length(); i++)

		{

			int val = str.charAt(i);

			if (char_set[val])

			{

				return false;

			}

			char_set[val] = true;

		}

		return true;

	}

	// Efficient program without additional memory space

	public static boolean isUniqueEfficient(String str)

	{

		int check = 0;

		for (int i = 0; i < str.length(); i++)

		{

			int val = str.charAt(i) - 'a';

			if ((check & (1 << val)) > 0)

			{

				return false;

			}

			check |= (1 << val);

		}

		return true;

	}

	public static void printOutput(boolean bool) {
		if (bool == false)

		{

			System.out.println(" There are duplicate characters in the string");

		}

		else

		{

			System.out.println(" The string contains only unique characters");

		}

	}

}