package com.programs;

/*
 * Java program to get the second largest element in an array
 */

public class secondLargest {

	public static int findSecondLargest(int arr[]) {
		int length = arr.length;
		int large, slarge, flag = 0;
		large = arr[0];
		slarge = arr[0];
		for (int i = 0; i < length; i++) {
			if (large < arr[i]) {
				large = arr[i];
			}
		}
		for (int i = 0; i < length; i++) {
			if (large != arr[i]) {
				slarge = arr[i];
				flag = 1;
				break;
			}
		}

		// to check whether the array contains only one value or not

		if (flag != 1) {
			slarge = large;
			return slarge;
		}
		for (int i = 0; i < length; i++) {
			if ((slarge < arr[i]) && (arr[i] < large)) {
				slarge = arr[i];
			}
		}
		return slarge;
	}
}