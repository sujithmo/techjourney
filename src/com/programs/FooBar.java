package com.programs;

/*
 * Given numbers from 1 to n.
 * - Print 'Foo" if number is divisible by 3
 * - Print "Bar" if number is divisible by 5
 * - Print "FooBar" if number is divisible by 15
 * - Else, print the number itself.
 * 
 * Implement it without using modulus operator
 */
public class FooBar {

	public static void main(String args[])
	{
		int num = 50;
		printFooBar(num);
	}
	
	public static void printFooBar(int num)
	{
		int foo = 3;
		int bar = 5;
		int foobar = 15;
		
		for(int i=1;i<=num;i++)
		{
			if(i==foobar)
			{
				System.out.println("FooBar");
				foobar = foobar+15;
				bar = bar +5;
				foo=foo+3;
			}
			
			else if(i==bar)
			{
				System.out.println("Bar");
				bar = bar +5;
			}
			
			else if(i==foo)
			{
				System.out.println("Foo");
				foo=foo+3;
			}
			else
			{
				System.out.println(i);
			}
		}
	}
}
