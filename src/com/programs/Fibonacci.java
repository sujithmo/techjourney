package com.programs;

/*
 * Java program to print fibonacci series upto a given number
 * Fibonacci Series starts from 0,1,1,2,3...
 */
public class Fibonacci {

	public static void main(String[] args) {
		
		printFibonacciSeries(13);
		printFibonacciSeries(0);
		printFibonacciSeries(2);
		printFibonacciSeries(5);
	}
	
	private static void printFibonacciSeries(int num)
	{
		int firstNum = 0;
		int secondNum = 1;
		
		System.out.println();
		System.out.println("Fibonacci Series upto number "+num+" is as follows: ");
		
		if(num<=0)
		{
			System.out.println("Erorr!! Enter a number greater than 0");
		}
		
		else if(num == 1)
		{
			System.out.println(firstNum);
		}
		
		else if (num == 2)
		{
			System.out.println(firstNum+" , "+secondNum);
		}
		
		else
		{
			System.out.print(firstNum+" , "+secondNum);
			int thirdNum = 0;
			//Starting from 2 since we had already printed first 2 numbers in the fibonacci series
			for(int index = 2; index<num;index++)
			{
				thirdNum = firstNum+secondNum;
				System.out.print(" , "+thirdNum);
				
				firstNum = secondNum;
				secondNum = thirdNum;
			}
			System.out.println();
		}
	}
}
