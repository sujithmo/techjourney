package com.programs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
 * Java program to print duplicate elements in an array
 */

public class dupList {

	public static void main(String args[])

	{

		int arr[] = { 1, 6, 4, 4, 4, 3, 6, 7, 2, 2, 9, 0, 13, 12, 1, 0, 4, 13,
				7 };

		Set<Integer> bset = new HashSet<Integer>();

		bset = returndup(arr);

		System.out.println(" The list of duplicate elements are as follows..");

		for (Integer i : bset)

		{

			System.out.print(i + "  ");

		}

	}

	public static Set<Integer> returndup(int arr[])

	{

		int len = arr.length;

		Map<Integer, Integer> map1 = new HashMap<Integer, Integer>();

		Set<Integer> set = new HashSet<Integer>();

		for (int i = 0; i < len; i++)

		{

			if (map1.containsKey(arr[i]))

			{

				map1.put(arr[i], map1.get(arr[i]) + 1);

			}

			else

			{

				map1.put(arr[i], 1);

			}

			if (map1.get(arr[i]) == 2)

			{

				set.add(arr[i]);

			}

		}

		return set;

	}

}