package com.programs;

import java.util.Random;

public class GenerateRandomNumber {

	public static void main(String[] args) {
		int count;
		Random rnum = new Random();

		System.out.println("10 Random Numbers between 1 and 1000:");
		System.out.println("***************");

		for (count = 1; count <= 10; count++) {
			System.out.println(rnum.nextInt(1000));
		}
	}
}
