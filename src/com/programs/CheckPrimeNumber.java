package com.programs;
/*
 * Java program to check whether a number is prime or not
 */
public class CheckPrimeNumber {

	public static void main(String[] args) {
		
		int num1 = 23;
		System.out.println(isPrimeNumber(num1));
		
		int num2 = 2;
		System.out.println(isPrimeNumber(num2));
		
		int num3 = 16;
		System.out.println(isPrimeNumber(num3));
		
		int num4 = 0;
		System.out.println(isPrimeNumber(num4));
		
		int num5 = -37;
		System.out.println(isPrimeNumber(num5));
		
	}
	
	public static boolean isPrimeNumber(int num)
	{
		if(num<=0)
		{
			return false;
		}
		
		else
		{
			for(int i=2;i<=num/2;i++)
			{
				if(num%i==0)
				{
					return false;
				}
			}
			return true;
		}
	}
}
