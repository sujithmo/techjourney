package com.programs;

/*
 * Java program to shuffle a decck of 52 cards
 */

public class ShuffleCards {

	public static void main(String args[])
	{
		int cards[] = new int[52];
		for(int i=0;i<52;i++)
		{
			cards[i]=i;
		}
		shuffleCards(cards);
		
		for(int i=0;i<52;i++)
		{
			System.out.print(cards[i]+ "  ");
		}
	}
	
	public static void shuffleCards(int cards[])
	{
		int temp, index;
		for(int i=0;i<cards.length; i++)
		{
			index = (int) (Math.random() * (cards.length-i)) + i;
			temp = cards[i];
			cards[i] = cards[index];
			cards[index] =temp;
		}
	}
}
